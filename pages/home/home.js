/**
 * 该页面主要功能是判断用户登录信息
 * 1.登录==主界面
 * 2.没有登录==登录界面
 */
Page({

  /**
   * 页面的初始数据
   */
  data: {
    pageIndex: 1,
    swipers: [],
    informations: [],
    carousels: [
      'http://img06.tooopen.com/images/20160818/tooopen_sy_175833047715.jpg']
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {

  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    //询问用户信息权限
    this.requestUserAuthorize();
    this.initSwipers();
    this.initTypes();
    let pageIndex = this.data.pageIndex;
    this.initData(pageIndex);
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
    let pageIndex = this.data.pageIndex;
    this.setData({
      pageIndex: 1
    })
    this.initData(1);
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
    console.log('onReachBottom')
    let pageIndex = this.data.pageIndex;
    pageIndex = pageIndex + 1;
    this.setData({
      pageIndex: pageIndex
    })
    this.initData(pageIndex);
  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  },


  /**
   * 用户授权测试
   * 1. wx.getSetting判断授权状态
   * 2.如果没有授权wx.authorize提示授权
   * 3.如果用户拒绝授权，可以转到用户设置界面，请求用户授权
   */
  requestUserAuthorize: function () {
    wx.getSetting({
      success: (response) => {
        console.log(response)
        if (!response.authSetting['scope.userInfo']) {
          wx.authorize({
            scope: 'scope.userInfo',
            success: (res) => {
              console.log(res)
              this.nextUserAuthorize();
            }
            , fail: () => {
              console.log('用户拒绝授权')
              //用户拒绝授权
              //关闭当前界面 
              wx.showModal({
                title: '提示',
                content: '该小程序需要使用到用户的信息，请确认权限',
                cancelText: '退出程序',
                confirmText: '允许权限',
                success: function (res) {
                  if (res.confirm) {
                    wx.openSetting({
                      success: (res) => {
                        console.log(res)
                      }
                    })
                  } else if (res.cancel) {
                    wx.navigateBack()
                  }
                }
              })
            }
          })
        } else {
          //用户已经授权
          console.log('用户已经授权')
          this.nextUserAuthorize();
        }
      }
    })
  },
  //用户授权以后
  nextUserAuthorize: function () {
    wx.getUserInfo({
      success: (res) => {
        this.setData({
          avatarUrl: res.userInfo.avatarUrl,
          nickName: res.userInfo.nickName
        });
      }
    })
  },

  /**
   * 用户的校验登录和登录操作，虽然不知道登录来干什么
   */
  loginUser: function () {
    console.log('进来这里了')
    wx.checkSession({
      success: function () {
        console.log('session 未过期，并且在本生命周期一直有效')
        console.log('wxCode=' + wx.getStorageSync('wxCode'))
      },
      fail: function () {
        console.log('session  过期，重新登录')
        wx.login({
          success: function (res) {
            console.log('code=' + res.code)
            console.log('errMsg=' + res.errMsg)
            if (res.code) {
              //发起网络请求
              console.log('获取用户登录态成功！')
              wx.setStorageSync('wxCode', res.code)
            } else {
              console.log('获取用户登录态失败！' + res.errMsg)
            }
          }
        });
      }
    })
  },
  /**
 * 功能区类型
 */
  initTypes: function () {
    let menus = [];
    let imageUrls = this.data.carousels;
    for (let i = 0; i < 10; i++) {
      let menu = {
        menuImage: imageUrls[i % 2],
        menuTitle: '标题' + i,
        menuType: '类型' + i,
        menuId: i
      }
      menus.push(menu)
    }
    this.setData({
      columns: 4,
      grids: menus
    })
  },

  /**
   * 初始化轮播的资源显示
   */
  initSwipers: function () {
    let swipers = [];
    let carousels = this.data.carousels;
    for (let i = 0; i < carousels.length; i++) {
      let swiper = {
        id: i,
        imageUrl: carousels[i]
      };
      swipers.push(swiper);
    }
    this.setData({
      swipers: swipers
    })
  },

  swipclickListener: function (event) {
    let swipers = this.data.swipers;
    let swiper = swipers[event.currentTarget.id];
    console.log(swiper.imageUrl)
    console.log(swiper)
    //TODO 点击事件发生
  },

  /**
   * 正常组件的点击事件发生
   */
  clickListener: function (event) {
    console.log(event)
    let url = '';
    switch (event.currentTarget.id) {
      case 'search':
        console.log('点击了搜索')
        url = '../search/search';
        break
      case 'userInfo':
        console.log('点击了用户')
        url = '../user-info/user-info?avatarUrl=' + this.data.avatarUrl + '&nickName=' + this.data.nickName;
        break
    }
    wx.navigateTo({
      url: url,
      success: (res) => {
        console.log(res);
      },
      fail: (res) => {
        console.log(res);
      }
    })
  },
  initData: function (pageIndex) {
    console.log('pageIndex=' + pageIndex)
    wx.request({
      url: 'http://113.105.74.135:8092/user/appStoreList',
      data: {
        'type': 0,
        'pageIndex': pageIndex,
        'token': 'undefined'
      },
      success: (res) => {
        var resluts = res.data.reslut;
        this.handlerData(pageIndex, resluts)
      }
    })
  },

  handlerData: function (pageIndex, resluts) {
    let informations = [];

    if (pageIndex > 1) {
      informations = this.data.informations;
    }
    if (resluts.length > 0) {
      for (let i = 0; i < resluts.length; i++) {
        var reslut = resluts[i];
        let information = {
          title: reslut.sc_companyname,
          subTitle: reslut.sc_address,
          price: reslut.sc_telephone,
          sellOut: reslut.sc_rank,
          distance: reslut.sc_industry,
          imageUrl: reslut.sc_imageurl
        };
        informations.push(information);
      }
      this.setData({
        informations: informations,
      })
    } else {
      wx.showToast({
        title: '没有更多数据',
        icon: "none", 
        duration: 2000
      });
      this.setData({
        pageIndex: pageIndex - 1
      })
    }
    wx.stopPullDownRefresh();
  },
  /**
   *当轮播图出现错误时候回调回来更新新的网址或是本地的错误图片
   */
  swiperImageError: function (event) {
    console.log(event.target.id)
    let swipers = this.data.swipers;
    let swiper = {
      id: event.target.id,
      imageUrl: 'http://img02.tooopen.com/images/20150928/tooopen_sy_143912755726.jpg'
    }
    //更替数组的元素
    swipers.splice(event.target.id, 1, swiper)
    this.setData({
      swipers: swipers
    })




  }
})