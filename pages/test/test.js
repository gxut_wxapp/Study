// pages/test/test.js
Page({

  /**
   * 页面的初始数据
   */
  data: {
  
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
  
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {
  
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    this.informations();
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {
  
  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {
  
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
  
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
  
  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {
  
  },

  informations: function () {
    let informations = [];
    let carousels = this.data.carousels;
    for (let i = 0; i < 10; i++) {
      let information = {
        title: '这个是标题' + i,
        subTitle: '这个是副标题这个是副标题这个是副标题这个是副标题' + i,
        price: i + 10,
        sellOut: i + 10,
        distance: 100 + i,
        imageUrl: 'http://img06.tooopen.com/images/20160818/tooopen_sy_175866434296.jpg'
      };
      informations.push(information);
    }
    this.setData({
      informations: informations
    })
  }
})