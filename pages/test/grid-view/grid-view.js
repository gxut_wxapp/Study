// pages/test/grid-view/grid-view.js
Page({

  /**
   * 页面的初始数据
   */
  data: {
    menus: [],
    imageUrls: [
      'https://timgsa.baidu.com/timg?image&quality=80&size=b9999_10000&sec=1520999618346&di=d598627df03137db236be244956008df&imgtype=0&src=http%3A%2F%2Fimgsrc.baidu.com%2Fimage%2Fc0%253Dpixel_huitu%252C0%252C0%252C294%252C40%2Fsign%3D0b7ad8f7f7039245b5b8e94feeecc1ae%2Fd043ad4bd11373f04ab641a5af0f4bfbfbed047a.jpg',
      'https://timgsa.baidu.com/timg?image&quality=80&size=b9999_10000&sec=1520999618343&di=23e5fdc50dce138729ed721545575a63&imgtype=0&src=http%3A%2F%2Fimgsrc.baidu.com%2Fimgad%2Fpic%2Fitem%2F3b292df5e0fe9925b75bdf6d3fa85edf8db171b3.jpg'
    ]
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {

  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {
    let s1 = '1';
    let s = parseInt(s1);
    let ccc = (s + 1) % 7 * 7 + 3;
    console.log("sss=" + ccc);
    this.initMenus();

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  },
  initMenus: function () {
    let menus = [];
    let imageUrls = this.data.imageUrls;
    for (let i = 0; i < 10; i++) {
      let menu = {
        menuImage: imageUrls[i % 2],
        menuTitle: '标题' + i,
        menuType: '类型'+i,
        menuId: i
      }
      menus.push(menu)
    }
    this.setData({
      columns: 4,
      grids: menus
    })
  }
})