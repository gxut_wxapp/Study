// pages/search/search.js
Page({

  /**
   * 页面的初始数据
   */
  data: {
    showLastInput: true
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {

  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    this.initData();
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  },
  initData: function () {
    wx.request({
      url: 'http://113.105.74.135:8092/user/appStoreList',
      data: {
        'type': 0,
        'pageIndex': 1,
        'token': 'undefined'
      },
      success: (res) => {
        var resluts = res.data.reslut;
        this.handlerData(resluts)
      }
    })
  },

  handlerData: function (resluts) {
    let informations = [];
    for (let i = 0; i < resluts.length; i++) {
      var reslut = resluts[i];
      let information = {
        title: reslut.sc_companyname,
        subTitle: reslut.sc_address,
        price: reslut.sc_telephone,
        sellOut: reslut.sc_rank,
        distance: reslut.sc_industry,
        imageUrl: reslut.sc_imageurl
      };
      informations.push(information);
    }
    this.setData({
      informations: informations,
      allInformations: informations
    })
  },
  inputChange: function (e) {
    var value = e.detail.value
    var pos = e.detail.cursor
    console.log(e)
    console.log("pos=" + pos)
    console.log('value=' + value)
    this.findByInput(value)
  },
  inputConfirm: function (e) {
    var value = e.detail.value
    var pos = e.detail.cursor
    console.log(e)
    console.log("pos=" + pos)
    console.log('value=' + value)
    wx.showToast({
      title: 'value=' + value,
    })
    wx.setStorageSync('lastSearch', value) 
    this.findByInput(value)
  },
  findByInput: function (value){
  let informations = [];
  let allInformations = this.data.allInformations;
  if (!value) {
    informations = allInformations;
  } else {
    for (let i = 0; i < allInformations.length; i++) {
      let allInformation = allInformations[i];
      if (allInformation.title.indexOf(value) > -1) {
        informations.push(allInformation);
      }
    }
  }
  this.setData({
    informations: informations,
  })
},
  inputFocusChange: function (e) {
    console.log(e)
    if (e.type == 'blur') {//失去焦点
      console.log('失去焦点')
      this.setData({
        showLastInput: true
      })
    } else {//得到焦点
      console.log('得到焦点')
      var values = wx.getStorageSync('lastSearch');
      console.log('values=' + values)
      this.setData({
        showLastInput: false || !values,
        lastInput: values
      })
    }
  },
  onClickListener: function (e) {
    switch (e.currentTarget.id) {
      case 'lastInput':
        let values = wx.getStorageSync('lastSearch');
        this.setData({
          inputStr: values
        })
        this.findByInput(values)
        break
      case 'clearInput':
        wx.setStorageSync('lastSearch', '')
        this.setData({
          showLastInput: true,
          lastInput: ''
        })
        this.findByInput('')
        break
    }
  }
})